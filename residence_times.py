import numpy as np

from expression import get_expression
from rate_matrix_MWC import make_rate_matrix_NEQ_MWC


def get_residence_times(M, V, Ip):
    # states = M.columns.values
    # Ib = []
    # In = []
    # Ibn = []
    # n = int((len(states[0]) - 1)/2)
    # # TODO: check if i got it right - такое ощущение, что все эти множества стейтов в итоге не нужны, а
    # # TODO: нужны лишь Ip - те, где надо считать residence times
    # for state in states:
    #     # if Mediator is ON or any of TFs is bound
    #     if state[0] == '1' or '1' in state[1:n + 1]:
    #         Ib.append(state)
    #         # no link attached in addition
    #         if '1' not in state[n+1:]:
    #             Ibn.append(state)
    #     # if Mediator is OFF or none of TFs is bound
    #     if state[0] == '0' or '1' not in state[1:n + 1]:
    #         In.append(state)

    # все это умножение на J c двух сторон уберет строки и стобцы, относящиеся к стейтам не в Ib,
    # а Ib - все стейты, где либо Медиатор ON, либо  ТФ связан, либо и то, и то
    # TODO: путаница с тем, в каких стейтах надо считать res time - в тольто тех, где
    # TODO: медиатор ON или в тех, где мед ON или ТФ связан (то есть множество Ib)
    # Jii = [1 if state in Ib else 0 for state in states]
    # J = np.diag(Jii)
    # -------------------
    # J = np.diag(Ip)
    # J = J[~np.all(J == 0, axis=1)]  # в статье авторы J описывают не так, как сами же делают, J не кв матрица
    # # так как она должна подвыбирать строки/столбцы, то есть дб как у меня в коде
    # Mw = np.dot(np.dot(J, M), np.transpose(J))
    # --------------------
    Mw = M[Ip].loc[:, Ip]

    m = Mw.shape[0]
    I_t = np.transpose(np.ones(m))
    # T = -I^T * Mw^{-1} - то, на что умножаем a в формуле для вычисления МО - стр 7
    T = - np.dot(I_t, np.linalg.inv(Mw))

    a = np.dot(M[Ip].loc[:, np.logical_not(Ip)], V[np.logical_not(Ip)])
    a = a / sum(a)

    m1T = np.dot(T, a)
    assert m1T > 0, f"rt = {m1T} and residence time cannot be <= 0!"
    return m1T


if __name__ == "__main__":
    c = 0.01
    k_bind_0 = 1
    k_unbind_S = 1e-2
    k_unbind_NS = 1

    k_bind = c * k_bind_0
    k_unbind = k_unbind_S
    k_bindM = 1e-2
    k_unbindM = 1e4

    alpha = 10
    k_link = 10
    k_unlink = 0

    n = 3
    M, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
    E, V = get_expression(M, Im)
    print("expression = ", E)

    mean_time = get_residence_times(M, V, Itf1)
    print('mean residence time = ', mean_time)


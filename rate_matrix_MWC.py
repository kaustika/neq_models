import pickle

import numpy as np
from itertools import combinations
import pandas as pd
import scipy.stats as stats
from os.path import exists

pd.options.mode.chained_assignment = None  # default='warn'

states_not_filtered = []


def get_binary_arrays(n, arr, i):
    global states_not_filtered
    if i==n:
        states_not_filtered.append(arr)
        return

    # First assign "0" at ith position
    # and try for all other permutations
    # for remaining positions
    arr_copy = arr.copy()
    arr_copy[i] = 0
    get_binary_arrays(n, arr_copy, i + 1)

    # And then assign "1" at ith position
    # and try for all other permutations
    # for remaining positions

    arr_copy = arr.copy()
    arr_copy[i] = 1
    get_binary_arrays(n, arr_copy, i + 1)


def count_number_of_states(n: int) -> int:
    # number of states
    m0 = pow(2, n)
    m1 = 0
    for i in range(n + 1):
        m1 += sum(1 for _ in combinations(range(n), i)) * pow(2, i)

    return m0 + m1


def generate_states(n: int):
    """
    нужно вектора состояний возможные сгенерировать и закрепить порядок - we order the states
    such that states with sM = 1 come first, followed by states sM = 0. (это лексикограф по идее)

    STATE VECTORS
    B = (sM, (s1,..., sn), (b1, ..., bn))
    проще всего, наверное, сгенерировать сначала все-все, а потом фильтрануть те, где линк не мб,
    так как нет ТФ связанного
    :param n: количество сайтов связывания
    :return:
    """
    if exists(str(n) + 'states.pkl'):
        with open(str(n) + 'states.pkl', 'rb') as pkl:
            return pickle.load(pkl)

    # TODO: исправить этот позор
    global states_not_filtered
    states_not_filtered = []
    get_binary_arrays(2 * n + 1, [None] * (2 * n + 1), 0)  # пишет в глобальную states_not_filtered

    # теперь отфильтровать так, чтобы там были только разрешенные macro-стейты
    states = []
    state_vector_length = len(states_not_filtered[0])
    for macrostate in states_not_filtered:
        is_forbidden = False
        for i in range(n + 1, state_vector_length):
            # линк мб образован только если ТФ связан
            if macrostate[i]==1 and macrostate[i - n]==0:
                is_forbidden = True
            # линк только из ON стейта мб образован
            if macrostate[i]==1 and macrostate[0]==0:
                is_forbidden = True

        if not is_forbidden:  # если никакой линк не появился без связанного ТФ, то стейт разрешен
            states.append(''.join(str(e) for e in macrostate))

    # закрепляем порядок за стейтами!
    states.reverse()

    # операция тяжелая, запишем в файл
    with open(str(n) + 'states.pkl', 'wb') as pkl:
        pickle.dump(states, pkl)

    return states


def get_state_filters(states, TF_numbers=None):
    if TF_numbers is None:
        TF_numbers = [1]
    Im = []  # по этим стейтам надо считать экспрессию
    Itfs = [[] for _ in range(len(TF_numbers))]  # по этим стейтам надо считать TF res time
    for state in states:
        if state[0]=='1':
            Im.append(True)
        else:
            Im.append(False)
        for i in range(len(TF_numbers)):
            TF_number = TF_numbers[i]
            Itf = Itfs[i]
            # не нужен сдвиг, тк как первым стоит стейт медиатора
            if state[TF_number]=='1':
                Itf.append(True)
            else:
                Itf.append(False)
    return Im, Itfs


def make_rate_matrix_NEQ_MWC(k_bind: float,
                             k_unbind: float,
                             k_bindM: float,
                             k_unbindM: float,
                             k_link: float,
                             k_unlink: float,
                             alpha: float,
                             n: int, symbol=False):
    m = count_number_of_states(n)
    states = generate_states(n)
    assert len(states)==m

    M_df = pd.DataFrame(np.zeros((m, m)))
    M_df.columns = states
    M_df.index = states

    # теперь пройтись по всем стейтам
    for state in states:
        # locate all linking and unlinking transitions
        for i in range(n + 1, 2*n + 1):  # state_vector_length = 2n + 1
            if state[i]=='0':  # i-th TF is not linked to Mediator
                continue
            else:
                # l=l[:index]+[‘new_value’]+l[index+1:]
                state_unlinked = state[:i] + '0' + state[i + 1:]
                M_df[state][state_unlinked] = 'k_unlink' if symbol else k_unlink
                M_df[state_unlinked][state] = 'k_link' if symbol else k_link

        # locate all binding and unbinding transitions
        for i in range(1, n + 1):
            if state[i]=='0':
                continue
            else:
                state_unbound_unlinked = state[:i] + '0' + state[i + 1:i + n] + '0' + state[i + n + 1:]
                # count removed links
                b = 1 if state[i + n]=='1' else 0
                M_df[state][state_unbound_unlinked] = 'k_unbind/a^b' if symbol else k_unbind / pow(alpha, b)
                if b==0:
                    M_df[state_unbound_unlinked][state] = 'k_bind' if symbol else k_bind

        # locate the states that are affected by Mediator switching
        if state[0]=='0':  # Mediator id OFF in original state
            continue
        else:
            state_off_unlinked = '0' + state[1:n + 1] + n * '0'  # mediator OFF and break all links
            b = len([elem for elem in state[n + 1:] if elem=='1'])
            M_df[state][state_off_unlinked] = 'k_unbindM/a^b' if symbol else k_unbindM / pow(alpha, b)
            if b==0:
                M_df[state_off_unlinked][state] = 'k_bindM' if symbol else k_bindM

    # At the end we set the diagonal values as minus sum of the columns:
    for state in states:
        M_df[state][state] = '-sum(M[column])' if symbol else -sum(M_df[state])

    Im, Itfs = get_state_filters(states)
    return M_df, Im, Itfs[0]


def make_rate_matrix_NEQ_MWC_K(k_bind: float,
                               k_unbind_list: list,
                               k_bindM: float,
                               k_unbindM: float,
                               k_link: float,
                               k_unlink: float,
                               alpha: float,
                               n: int, symbol=False):
    """
    Here we pass a list of k_unbind rates for each of n TFs.
    :param k_bind:
    :param k_unbind_list:
    :param k_bindM:
    :param k_unbindM:
    :param k_link:
    :param k_unlink:
    :param alpha:
    :param n:
    :param symbol:
    :return:
    """

    assert len(k_unbind_list) == n

    m = count_number_of_states(n)
    states = generate_states(n)
    assert len(states) == m

    M_df = pd.DataFrame(np.zeros((m, m)))
    M_df.columns = states
    M_df.index = states

    # теперь пройтись по всем стейтам
    for state in states:
        # locate all linking and unlinking transitions
        for i in range(n + 1, 2 * n + 1):  # state_vector_length = 2n + 1
            if state[i]=='0':  # i-th TF is not linked to Mediator
                continue
            else:
                # l=l[:index]+[‘new_value’]+l[index+1:]
                state_unlinked = state[:i] + '0' + state[i + 1:]
                M_df[state][state_unlinked] = 'k_unlink' if symbol else k_unlink
                M_df[state_unlinked][state] = 'k_link' if symbol else k_link

        # locate all binding and unbinding transitions
        for i in range(1, n + 1):
            k_unbind = k_unbind_list[i - 1]
            if state[i]=='0':
                continue
            else:
                state_unbound_unlinked = state[:i] + '0' + state[i + 1:i + n] + '0' + state[i + n + 1:]
                # count removed links
                b = 1 if state[i + n]=='1' else 0
                M_df[state][state_unbound_unlinked] = 'k_unbind_{}/a^b'.format(i) if symbol else k_unbind / pow(alpha, b)
                if b==0:
                    M_df[state_unbound_unlinked][state] = 'k_bind' if symbol else k_bind

        # locate the states that are affected by Mediator switching
        if state[0]=='0':  # Mediator id OFF in original state
            continue
        else:
            state_off_unlinked = '0' + state[1:n + 1] + n * '0'  # mediator OFF and break all links
            b = len([elem for elem in state[n + 1:] if elem=='1'])
            M_df[state][state_off_unlinked] = 'k_unbindM/a^b' if symbol else k_unbindM / pow(alpha, b)
            if b==0:
                M_df[state_off_unlinked][state] = 'k_bindM' if symbol else k_bindM

    # At the end we set the diagonal values as minus sum of the columns:
    for state in states:
        M_df[state][state] = '-sum(M[column])' if symbol else -sum(M_df[state])

    # получили фильтры состояний для каждого ТФ, тк тут уже НЕ для всех
    # время пребывания будет одно и то же
    Im, Itfs = get_state_filters(states, TF_numbers=[i for i in range(1, n + 1)])

    return M_df, Im, Itfs


def sample_n_k_unbind_rates(n, k_unbind_S=1e-2, k_unbind_NS=1):
    """
    # я понимаю, что это должно быть какое-то усеченное эксп распределение с фикс концами, но
    # не понимаю, какой брать параметр распределения
    # TODO: нелогично как-то, ведь концентрация подразумевает однотипность мб, но мб и ок, ведь
    # они прилипают случайно в некоторой мере, но так или иначе
    # TODO: надо запрогать подстаивание концентрации под заданный уровень экспрессии
    :param alpha:
    :param n:
    :param k_unbind_S:
    :param k_unbind_NS:
    :return:
    """
    scale = 1
    lower, upper = 1/((k_unbind_NS + k_unbind_S)/2), 1/k_unbind_S
    X = stats.truncexpon(b=(upper - lower) / scale, loc=lower, scale=scale)
    data = X.rvs(n - 2)
    data = np.append(data, [lower, upper])
    data = [1/k_u for k_u in data]
    data.sort()  # ascending -> smaller rates come first = stronger sites come first
    return list(data)


if __name__=="__main__":
    c = 0.01
    k_bind_0 = 1
    k_unbind_S = 1e-2
    k_unbind_NS = 1

    k_bind = c * k_bind_0
    k_unbind = k_unbind_S
    k_bindM = 1e-2
    k_unbindM = 1e4

    alpha = 10
    k_link = 10
    k_unlink = 0

    n = 6
    # M, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
    # print(M)

    k_unbind_list = sample_n_k_unbind_rates(n)
    print(k_unbind_list)
    # M, Im, Itf1 = make_rate_matrix_NEQ_MWC_K(k_bind, k_unbind_list, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
    # print(M)

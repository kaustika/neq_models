# TODO: как они фиксируют экспрессию?
# как фиксируют концентрацию понятно, c участвует в формуле для рейта связывания, а экспрессия где?
# проверить, что все совпадает с матлабом при большем числе сайтов (не забыть про Ip - его в млабе надо иначе задавать тогда)
import argparse
import math

from expression import get_expression
from rate_matrix_MWC import make_rate_matrix_NEQ_MWC_K, sample_n_k_unbind_rates, make_rate_matrix_NEQ_MWC
from residence_times import get_residence_times
import numpy as np
from tqdm import tqdm
import pickle
from statistics import mean
from math import log10
import matplotlib.pyplot as plt
import sys
from itertools import compress

def run_experiment(Np, n, Ns):
    # those parameters stay fixed, values from page 3
    c = 0.01
    k_bind_0 = 1
    k_bind = c * k_bind_0
    k_bindM = 1e-2
    k_unbindM = 1e4
    k_unlink = 0

    k_unbind_S = 1e-2
    k_unbind_NS = 1
    # those parameters will be changed - alpha, k_link
    # For exploration of the phase space we use alpha in [1, 10^10], k_link in [10^-8, 10^8]

    # GRID
    timescale = 1 / k_unbind_S

    grid = []
    for alpha in np.logspace(0, 10, num=Np):
        for k_link in np.logspace(-8, 8, num=Np):
            grid.append((alpha, k_link))

    with open(f'n{n}_samples.txt', 'w') as f:
        samples = []
        for sample in range(1, Ns + 1):
            k_unbind_list = sample_n_k_unbind_rates(n, k_unbind_S, k_unbind_NS)
            samples.append(k_unbind_list)
            print(k_unbind_list)
            f.write(' '.join([str(k) for k in k_unbind_list]))

    # для текущего ансамбля получаем значения в каждой точке сетки:
    # (S = E_ensemble/E_NS, T_TF/T0 = mean(meanT_TF_i/T_0))
    # далее в каждой точке сетки усредняем по сэмплам

    experiment = []
    for alpha, k_link in tqdm(grid):
        S_for_samples = []
        E_for_samples = []
        T_for_samples = []
        for k_unbind_list in samples:
            M_S, Im, Itfs = make_rate_matrix_NEQ_MWC_K(k_bind, k_unbind_list, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
            E_S, V_S = get_expression(M_S, Im)

            mean_times_S = []
            for i in range(n):
                rt = get_residence_times(M_S, V_S, Itfs[i])
                assert rt > 0, (str(rt), str(Itfs[i]))
                mean_times_S.append(rt)

            mean_time_S = mean(mean_times_S)  # усреднение по сайтам средних времен пребывания

            M_NS, Im, _ = make_rate_matrix_NEQ_MWC(k_bind, k_unbind_NS, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
            E_NS, V_NS = get_expression(M_NS, Im)

            S_for_samples.append(E_S/E_NS)
            T_for_samples.append(mean_time_S/timescale)
            E_for_samples.append(E_S)
        experiment.append((S_for_samples, T_for_samples, E_for_samples))

    filename = f'experiment_Np{Np}_n{n}_T0{timescale}_num_samples{Ns}_v2'
    with open(filename + '.pkl', 'wb') as pkl:
        pickle.dump(experiment, pkl)
    return filename


def plot_2A_ensemble(experiment, filename):
    specificities, res_times, E = zip(*experiment)
    # print('max(res_times) = ', max(res_times))
    # print('max(specificities) = ', max(specificities))
    # print('max S theoretical = ',   1e4/1e-2)
    # print(min(mean(res_times)))
    # print(max(mean(res_times)))
    # rt_too_small_mask = [mean(rt) < sys.float_info.min for rt in res_times]
    # res_times = list(compress(res_times, rt_too_small_mask))
    # specificities = list(compress(specificities, rt_too_small_mask))
    # E = list(compress(E, rt_too_small_mask))
    # мб нужно не фильтровать маленькие, а заменять на min float?
    for rt in res_times:
        assert mean(rt) > 0, str(rt)
        for t in rt:
            assert t > 0, str(t)

    for s in specificities:
        assert mean(s) > 0, str(s)
        for sp in s:
            assert sp > 0, str(sp)
    plt.scatter(
        [log10(mean(rt)) for rt in res_times],
        [log10(mean(s)) for s in specificities],
        c=[mean(e) for e in E], cmap='spring')
    # plt.scatter([log10(rt) for rt in res_times], [log10(s) for s in specificities])
    # plt.axhline(1, color='r')  # horizontal
    # plt.axhline(k_unbindM/k_bindM, color='r')  # horizontal
    # plt.axvline(log10(1/k_unbind_S), color='r')  # vertical
    plt.xlabel("$log_{10}(T_{TF}/T_0)$")
    plt.ylabel("$log_{10}(S)$")
    plt.title("Fixed c = c0")
    cb = plt.colorbar()
    cb.set_label('E')
    # plt.show()
    plt.savefig(filename + '.png')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='Plot graph 2A (ensemble) from main article')
    parser.add_argument('--Np', action='store')
    parser.add_argument('-n', action='store')
    parser.add_argument('--Ns', action='store')
    parser.add_argument('--filename', action='store', default=None)
    args = parser.parse_args()

    filename = run_experiment(Np=int(args.Np), n=int(args.n), Ns=int(args.Ns))
    # filename = run_experiment(Np=30, n=7, Ns=3)
    # filename = args.filename
    # filename = "experiment_Np30_n8_T0100.0_num_samples10"
    with open(filename +'.pkl', 'rb') as pkl:
        experiment = pickle.load(pkl)

    plot_2A_ensemble(experiment, filename)




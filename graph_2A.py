# TODO: как они фиксируют экспрессию?
# как фиксируют концентрацию понятно, c участвует в формуле для рейта связывания, а экспрессия где?
# проверить, что все совпадает с матлабом при большем числе сайтов (не забыть про Ip - его в млабе надо иначе задавать тогда)
import argparse
import math
from statistics import mean

from expression import get_expression
from rate_matrix_MWC import make_rate_matrix_NEQ_MWC
from residence_times import get_residence_times
import numpy as np
from tqdm import tqdm
import pickle
from math import log10
import matplotlib.pyplot as plt


def run_experiment(Np, n):
    # those parameters stay fixed, values from page 3
    c = 0.01
    k_bind_0 = 1
    k_bind = c * k_bind_0
    k_bindM = 1e-2
    k_unbindM = 1e4
    k_unlink = 0

    k_unbind_S = 1e-2
    k_unbind_NS = 1
    # those parameters will be changed - alpha, k_link
    # For exploration of the phase space we use alpha in [1, 10^10], k_link in [10^-8, 10^8]

    # GRID
    # Np = 10
    # n = 3
    timescale = 1 / k_unbind_S

    grid = []
    for alpha in np.logspace(0, 10, num=Np):
        for k_link in np.logspace(-8, 8, num=Np):
            grid.append((alpha, k_link))

    experiment = []
    for alpha, k_link in tqdm(grid):
        M_S, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind_S, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
        E_S, V_S = get_expression(M_S, Im)
        # нужно передать туда те состояния, где 1ый сайт связан
        mean_time_S = get_residence_times(M_S, V_S, Itf1)
        assert mean_time_S > 0, "mean time cannot be negative"
        # print("Expression from specific = ", E_S)
        # print("Mean residence time for specific = ", mean_time_S)

        M_NS, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind_NS, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
        E_NS, V_NS = get_expression(M_NS, Im)
        # mean res time for the NS sites is not included in the graph(?)
        # mean_time_NS = get_residence_times(M_NS, V_NS, Ip_NS)
        # print("Expression from non-specific = ", E_NS)
        # print("Mean residence time for non-specific = ", mean_time_NS)

        S = E_S/E_NS
        # if S == 1:
        #     print(alpha, k_link)
        #     print("V_S full = ", V_S)
        #     print("V_NS full = ", V_NS)
        #     print("V_S for ON states = ", V_S[Im])
        #     print("V_NS for ON states = ", V_NS[Im])
        #     print("Expressions: ", sum(V_S[Im]), sum(V_NS[Im]))

        experiment.append((S, mean_time_S/timescale, E_S))

    filename = f'experiment_Np{Np}_n{n}_T0{timescale}_v2'
    with open(filename + '.pkl', 'wb') as pkl:
        pickle.dump(experiment, pkl)
    return filename


def plot_2A(experiment, filename):
    specificities, res_times, E = zip(*experiment)
    for rt in res_times:
        assert rt > 0, str(rt)

    for s in specificities:
        assert s > 0, str(s)

    # print('max(res_times) = ', max(res_times))
    # print('max(specificities) = ', max(specificities))
    # print('max S theoretical = ',   1e4/1e-2)
    plt.scatter([log10(rt) for rt in res_times], [log10(s) for s in specificities], c=E, cmap='winter')
    # plt.scatter([log10(rt) for rt in res_times], [log10(s) for s in specificities])
    # plt.axhline(1, color='r')  # horizontal
    # plt.axhline(k_unbindM/k_bindM, color='r')  # horizontal
    # plt.axvline(log10(1/k_unbind_S), color='r')  # vertical
    plt.xlabel("$log_{10}(T_{TF}/T_0)$")
    plt.ylabel("$log_{10}(S)$")
    plt.title("Fixed c = c0")
    cb = plt.colorbar()
    cb.set_label('E')
    # plt.show()
    plt.savefig(filename + '.png')


if __name__ == "__main__":
    # k_bindM = 1e-2
    # k_unbindM = 1e4
    # k_unbind_S = 1e-2
    parser = argparse.ArgumentParser(prog='Plot graph 2A from main article')
    parser.add_argument('--Np', action='store')
    parser.add_argument('-n', action='store')
    args = parser.parse_args()

    filename = run_experiment(Np=int(args.Np), n=int(args.n))
    with open(filename + '.pkl', 'rb') as pkl:
        experiment = pickle.load(pkl)

    plot_2A(experiment, filename)



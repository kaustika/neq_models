import argparse
import os
import pickle

from matplotlib import pyplot as plt

from graph_2A_ensemble import plot_2A_ensemble

from graph_2A import plot_2A


def plot_2A_together(n, Np, Ns):
    base_file = os.path.join("experiments", f"experiment_Np{Np}_n{n}_T0100.0.pkl")
    ensemble_file = os.path.join("experiments", f"experiment_Np{Np}_n{n}_T0100.0_num_samples{Ns}.pkl")
    with open(base_file, 'rb') as pkl:
        experiment_base = pickle.load(pkl)

    with open(ensemble_file, 'rb') as pkl:
        experiment_ensemble = pickle.load(pkl)

    filename=os.path.join("experiments", f"experiment_Np{Np}_n{n}_T0100.0_comparison")
    plot_2A(experiment_base, filename)
    plot_2A_ensemble(experiment_ensemble, filename)
    # specificities_base, res_times_base, E_base = zip(*experiment_base)
    # specificities_ens, res_times_ens, E_ens = zip(*experiment_ensemble)
    # num_points = len(experiment_base)
    # specificities_base = list(specificities_base)
    # res_times_base = list(res_times_base)
    # E_base = list(E_base)
    # line = []
    # for i in range(num_points):
    #     s_base = specificities_base[i]
    #     rt_base = res_times_base[i]
    #     e_base = E_base[i]
    #     if e_base <= 0.57 and e_base >= 0.43:
    #        line.append((s_base, rt_base, e_base))
    # print(line)
    # one_base =
    # two_base =
    # one_ens =
    # two_ens =
    plt.show()


if __name__=="__main__":
    parser = argparse.ArgumentParser(prog='Plot graph 2A from main article')
    parser.add_argument('--Np', action='store', default=30)
    parser.add_argument('-n', action='store')
    parser.add_argument('--Ns', action='store', default=10)
    args = parser.parse_args()

    #plot_2A_together(int(args.n), int(args.Np), int(args.Ns))
    plot_2A_together(6, 30, 10)

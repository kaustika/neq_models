import argparse
from expression import get_expression
from rate_matrix_MWC import make_rate_matrix_NEQ_MWC
from residence_times import get_residence_times
import numpy as np
from tqdm import tqdm
import pickle
from math import log10, log, exp
import matplotlib.pyplot as plt
from scipy.optimize import root_scalar


# As in 2A, but the TF concentration at each point in the phase space is adjusted to hold average expression
# fixed at E=0.5


def expression_opt(k_bind, E0, args):
    # явно работает как-то не так..
    M, Im, _ = make_rate_matrix_NEQ_MWC(exp(k_bind), *args)
    E, _ = get_expression(M, Im)
    return E - E0


def optimize_concentration(E0, args, B1, B2):
    # typeof(args) = tuple

    def e_opt(k_bind):
        return expression_opt(k_bind, E0, args)

    solution = root_scalar(e_opt, method='brentq', bracket=[B1, B2], xtol=1e-16)
    return exp(solution.root)


def run_experiment(Np, n, E0):
    # those parameters stay fixed, values from page 3
    k_bindM = 1e-2
    k_unbindM = 1e4
    k_unlink = 0

    k_unbind_S = 1e-2
    k_unbind_NS = 1
    # those parameters will be changed - alpha, k_link
    # For exploration of the phase space we use alpha in [1, 10^10], k_link in [10^-8, 10^8]
    B1 = -20 * log(10)
    B2 = 10 * log(10)

    # GRID
    timescale = 1 / k_unbind_S

    grid = []
    for alpha in np.logspace(0, 10, num=Np):
        for k_link in np.logspace(-8, 8, num=Np):
            grid.append((alpha, k_link))

    experiment = []
    for alpha, k_link in tqdm(grid):
        args_S = (k_unbind_S, k_bindM, k_unbindM, k_link, k_unlink, alpha, n)
        if expression_opt(B1, E0, args_S) * expression_opt(B2, E0, args_S) < 0:
            k_bind = optimize_concentration(E0, args_S, B1, B2)
            M_S, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind_S, k_bindM, k_unbindM, k_link, k_unlink, alpha, n)
            E_S, V_S = get_expression(M_S, Im)
            print(E_S)
            # нужно передать туда те состояния, где 1ый сайт связан
            mean_time_S = get_residence_times(M_S, V_S, Itf1)
            # print("Expression from specific = ", E_S)
            # print("Mean residence time for specific = ", mean_time_S)

            M_NS, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind_NS, k_bindM, k_unbindM, k_link, k_unlink, alpha, n)
            E_NS, V_NS = get_expression(M_NS, Im)
            # mean res time for the NS sites is not included in the graph(?)
            # mean_time_NS = get_residence_times(M_NS, V_NS, Ip_NS)
            # print("Expression from non-specific = ", E_NS)
            # print("Mean residence time for non-specific = ", mean_time_NS)

            S = E_S / E_NS
            # if S == 1:
            #     print(alpha, k_link)
            #     print("V_S full = ", V_S)
            #     print("V_NS full = ", V_NS)
            #     print("V_S for ON states = ", V_S[Im])
            #     print("V_NS for ON states = ", V_NS[Im])
            #     print("Expressions: ", sum(V_S[Im]), sum(V_NS[Im]))

            experiment.append((S, mean_time_S / timescale, E_S))

    filename = f'experiment_Np{Np}_n{n}_T0{timescale}_E0'
    with open(filename + '.pkl', 'wb') as pkl:
        pickle.dump(experiment, pkl)
    return filename


def plot_2C(experiment):
    specificities, res_times, E = zip(*experiment)
    print(max(E), min(E))
    # print('max(res_times) = ', max(res_times))
    # print('max(specificities) = ', max(specificities))
    # print('max S theoretical = ',   1e4/1e-2)
    # plt.scatter([log10(rt) for rt in res_times], [log10(s) for s in specificities], c=E, cmap='gnuplot')
    plt.scatter([log10(rt) for rt in res_times], [log10(s) for s in specificities])
    # plt.axhline(1, color='r')  # horizontal
    # plt.axhline(k_unbindM/k_bindM, color='r')  # horizontal
    # plt.axvline(log10(1/k_unbind_S), color='r')  # vertical
    plt.xlabel("$log_{10}(T_{TF}/T_0)$")
    plt.ylabel("$log_{10}(S)$")
    plt.title("Fixed E = E0")
    cb = plt.colorbar()
    cb.set_label('E')
    plt.show()
    plt.savefig(filename + '.png')


if __name__=="__main__":
    # k_bindM = 1e-2
    # k_unbindM = 1e4
    # k_unbind_S = 1e-2
    parser = argparse.ArgumentParser(prog='Plot graph 2A from main article')
    parser.add_argument('--Np', action='store')
    parser.add_argument('-n', action='store')
    parser.add_argument('--E0', action='store', default=0.5)
    args = parser.parse_args()

    # filename = run_experiment(Np=int(args.Np), n=int(args.n), E0=float(args.E0))
    filename = 'experiment_Np30_n3_T0100.0_E0'
    with open(filename + '.pkl', 'rb') as pkl:
        experiment = pickle.load(pkl)

    plot_2C(experiment)

import numpy as np
from rate_matrix_MWC import make_rate_matrix_NEQ_MWC


def get_expression(M, Im):
    # Reduced matrix(M is degenerate)
    m = M.shape[0]
    A1 = np.c_[np.eye(m - 1), np.zeros((m - 1, 1))]
    A0 = np.r_[np.eye(m - 1), -np.ones((1, m - 1))]
    Mr = np.dot(np.dot(A1, M), A0)

    # Compute the steady state occupancies by solving the master
    # equation with lhs (time derivative) set to zero (S33): Mr*V + f = 0

    # The vector f is given by the first m - 1 terms of the last column of M:
    f = M.iloc[:, -1][:-1].values
    V = np.linalg.solve(Mr, -f)
    # The occupancy of the last state m is thus given by 1 - sum(V)
    V = np.append(V, 1 - sum(V))
    # I(s_M = 1) is the indicator function which is 1 if the Mediator
    # is ON in state indexed with j and zero otherwise (pp 16) -> we can
    # compute it right away i guess as we know in which states Mediator is ON

    # Im = I(s_M = 1), the naming after the code for the paper.
    # formula - pp 16: E = sum_{j = 1}^m V_j * I(s_M = 1)
    E = sum(V[Im])
    return E, V


if __name__ == "__main__":
    c = 0.01
    k_bind_0 = 1
    k_unbind_S = 1e-2
    k_unbind_NS = 1

    k_bind = c * k_bind_0
    k_unbind = k_unbind_S
    k_bindM = 1e-2
    k_unbindM = 1e4

    alpha = 10
    k_link = 10
    k_unlink = 0

    n = 1
    M, Im, Itf1 = make_rate_matrix_NEQ_MWC(k_bind, k_unbind, k_bindM, k_unbindM, k_link, k_unlink, alpha, n, symbol=False)
    E, V = get_expression(M, Im)
    print(E)
